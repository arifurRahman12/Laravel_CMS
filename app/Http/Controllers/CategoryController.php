<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests\Category\CreateCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::all();
        return view('category.index', compact('categories', $categories));
    }

    public function create()
    {
        return view('category.create');
    }


    public function store(CreateCategoryRequest $request)
    {
        Category::create([
            'name' => $request->name
        ]);

        session()->flash('success', 'New Category Added');

        return redirect( route('category.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit(Category $category)
    {
        return view('category.create', compact('category', $category));
    }

    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->update([
           'name' => $request->name
        ]);

        session()->flash('success', 'Category Updated');

        return redirect(route('category.index'));
    }

    public function destroy(Category $category)
    {
        if($category->posts->count() > 0){
            session()->flash('error', 'this category can not be deleted because it is associated with some posts');

            return redirect()->back();
        }

        $category->delete();

        session()->flash('success', 'Category Deleted');

        return redirect(route('category.index'));
    }
}
