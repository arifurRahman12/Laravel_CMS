<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Http\Requests\Tag\CreateTagRequest;
use App\Http\Requests\Tag\UpdateTagRequest;

class TagController extends Controller
{

    public function index()
    {
        return view('tag.index')->with('tags', Tag::all());
    }

    public function create()
    {
        return view('tag.create');
    }


    public function store(CreateTagRequest $request)
    {
        Tag::create([
            'name' => $request->name
        ]);

        session()->flash('success', 'New Tag Added');

        return redirect( route('tag.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit(Tag $tag)
    {
        return view('Tag.create')->with('tag', $tag);
    }

    public function update(UpdateTagRequest $request, Tag $tag)
    {
        $tag->update([
            'name' => $request->name
        ]);

        session()->flash('success', 'Tag Updated');

        return redirect(route('tag.index'));
    }

    public function destroy(Tag $tag)
    {
        if($tag->posts->count() > 0){
            session()->flash('error', 'this tag can not be deleted because it has some post associated with it');

            return redirect()->back();
        }

        $tag->delete();

        session()->flash('success', 'Tag Deleted');

        return redirect(route('tag.index'));
    }
}
