<?php

use Illuminate\Database\Seeder;
use App\Post;
use App\Category;
use App\tag;
use App\User;
use Illuminate\Support\Facades\Hash;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $author1 = User::create([
            'name' => 'Arifur',
            'email' => 'arifurrahman1427@gmail.com',
            'password' => Hash::make('password')
        ]);

        $author2 = User::create([
            'name' => 'Sajidur',
            'email' => 'sajid@gmail.com',
            'password' => Hash::make('password')
        ]);

        $category1 = Category::create([
            'name' => 'News'
        ]);

        $category2 = Category::create([
            'name' => 'Design'
        ]);

        $category3 = Category::create([
            'name' => 'Partnership'
        ]);

        $category4 = Category::create([
            'name' => 'Hiring'
        ]);

        $post1 = Post::create([
            'title' => 'We relocated our office to a new designed garage',
            'description' => 'The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again',
            'content' => 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Ciceros De Finibus Bonorum et Malorum for use in a type specimen book. It usually begins with',
            'image' => 'posts/1.jpg',
            'category_id' => $category1->id,
            'user_id' => $author1->id // You can use alternate way for this see immediate below
        ]);

        $post2 = $author2->posts()->create([
            'title' => 'Top 5 brilliant content marketing strategies',
            'description' => 'The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again',
            'content' => 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Ciceros De Finibus Bonorum et Malorum for use in a type specimen book. It usually begins with',
            'image' => 'posts/2.jpg',
            'category_id' => $category2->id
        ]);

        $post3 = $author1->posts()->create([
            'title' => 'Best practices for minimalist design with example',
            'description' => 'The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again',
            'content' => 'Lorem ipsu or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Ciceros De Finibus Bonorum et Malorum for use in a type specimen book. It usually begins with',
            'image' => 'posts/3.jpg',
            'category_id' => $category3->id
        ]);

        $post4 = $author2->posts()->create([
            'title' => 'Congratulate and thank to Maryam for joining our team',
            'description' => 'The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again',
            'content' => 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Ciceros De Finibus Bonorum et Malorum for use in a type specimen book. It usually begins with',
            'image' => 'posts/4.jpg',
            'category_id' => $category4->id
        ]);

        $tag1 = Tag::create([
            'name' => 'Job'
        ]);

        $tag2 = Tag::create([
            'name' => 'Customer'
        ]);

        $tag3 = Tag::create([
            'name' => 'Design'
        ]);

        $post1->tags()->attach([$tag1->id, $tag2->id]);
        $post2->tags()->attach([$tag2->id, $tag3->id]);
        $post3->tags()->attach([$tag3->id, $tag1->id]);
        $post4->tags()->attach([$tag1->id]);
    }
}
