@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        {{ isset($category) ? 'Edit Category' : 'Create Category' }}
    </div>
    <div class="card-body">
        <form action="{{ isset($category) ? route('category.update', $category->id) : route('category.store') }}" method="POST">
            @csrf
            @if(isset($category))
                @method("PUT")
            @endif
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" class="form-control" id="name" value="{{ isset($category) ? $category->name : '' }}">
            </div>
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <button class="btn btn-success" type="submit">{{ isset($category) ? 'Update' : 'Add Category'}}</button>
        </form>
    </div>
</div>
@endsection
