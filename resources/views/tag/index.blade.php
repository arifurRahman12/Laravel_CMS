@extends('layouts.app')

@section('content')
    <div class="d-flex justify-content-end mb-2">
        <a href="{{ route('tag.create') }}" class="btn btn-success">Add tag</a>
    </div>

    <div class="card mb-3">
        <div class="card-header">Tags</div>
        <div class="card-body">
            @if($tags->count() > 0)
                <table class="table table-hover">
                    <thead class="thead-dark">
                    <th>Name</th>
                    <th>Posts Count</th>
                    <th></th>
                    </thead>
                    <tbody>
                    @foreach($tags as $tag)
                        <tr>
                            <td>{{ $tag->name }}</td>
                            <td> {{ $tag->posts->count() }} </td>
                            <td>
                                <a href="{{ route('tag.edit', $tag->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                <button type="button" onclick="handleDelete({{ $tag->id }})" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center">No Tags Yet</h3>
            @endif

            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form action="" method="POST" id="deletetagForm">
                        @csrf
                        @method('DELETE')
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="deleteModalLabel">Delete tag</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="text-center font-weight-bold">Are you sure you want to delete this tag?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-danger">Confirm</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function handleDelete(id){
            var form = document.getElementById('deletetagForm');
            form.action = '/tag/' + id;
            $('#deleteModal').modal('show');
        }
    </script>
@endsection
