@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            {{ isset($tag) ? 'Edit tag' : 'Create tag' }}
        </div>
        <div class="card-body">
            <form action="{{ isset($tag) ? route('tag.update', $tag->id) : route('tag.store') }}" method="POST">
                @csrf
                @if(isset($tag))
                    @method("PUT")
                @endif
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" value="{{ isset($tag) ? $tag->name : '' }}">
                </div>
                @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button class="btn btn-success" type="submit">{{ isset($tag) ? 'Update' : 'Add tag'}}</button>
            </form>
        </div>
    </div>
@endsection
