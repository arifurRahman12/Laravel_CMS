<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Blog\PostController;

Route::get('/', 'WelcomeController@index')->name('welcome');
Route::get('/blog/posts/{post}', [PostController::class, 'show'])->name('blog.show');
Route::get('/blog/categories/{category}', [PostController::class, 'category'])->name('blog.category');
Route::get('/blog/tags/{tag}', [PostController::class, 'tag'])->name('blog.tag');

Auth::routes();

Route::middleware('auth')->group(function (){

    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('category', 'CategoryController');

    Route::resource('post', 'PostController')->middleware('auth');

    Route::resource('tag', 'TagController');

    Route::get('/trashed-posts', 'PostController@trashed')->name('trashed-posts.index');

    Route::put('/restore-post/{post}', 'PostController@restore')->name('restore-post');
});

    Route::middleware(['auth', 'admin'])->group(function (){
        Route::get('/users/profile', 'UserController@edit')->name('user.edit-profile');
        Route::put('/users/profile', 'UserController@update')->name('user.update-profile');
        Route::get('/users', 'UserController@index')->name('users.index');
        Route::post('/users/{user}/make-admin', 'UserController@makeAdmin')->name('user.make-admin');
    });
